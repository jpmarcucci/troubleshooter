FROM alpine:latest

RUN apk add nmap mtr bind-tools tcpdump mysql-client curl

CMD ["/usr/bin/tail", "-f", "/dev/null"]